### Getting Started
This is a React-Mobx based project which will let you get started with Mobx in React with all needfull configuration
embedded in it. 
Clone this repository using  https://rahulranjanoz@bitbucket.org/rahulranjanoz/react-mobx.git

Run "npm install" to install node_modules.
After installing run "npm start"
Happy React-Mobx Hacking!!