import React, { Component } from 'react';
import './App.css';
import { inject, observer } from "mobx-react";

@inject('TodoStore')
@observer
class App extends Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleSubmit(e) {
    e.preventDefault();
    const todo = this.todo.value;
    this.props.TodoStore.addTodos(todo);
    this.todo.value = "";
  }
  render() {
    const { TodoStore } = this.props;
    return (
      <div className="App">
        <h2>You have {TodoStore.todosCount} tasks.</h2>
        <form onSubmit={e => this.handleSubmit(e)}>
          <input type="text" placeholder="Enter Task" ref={input => this.todo =
            input} />
          <button>Add Task</button>
        </form>
        <ul>
          {TodoStore.todos.map((todo, i) => (
            <li key={i}>
              {todo}
            </li>
          ))}
        </ul>
      </div >
    );
  }
}

export default App;
