import { observable, autorun, computed, action } from "mobx";

class TodoStore {
    @observable todos = ["buy milk"]
    @observable filter = ""

    @action addTodos = (todo) => {
        this.todos.push(todo);
    }
    @computed get todosCount() {
        return this.todos.length;
    }
}
var Store = window.Store = new TodoStore()

export default Store;


autorun(() => {
    console.log(Store.filter);
})