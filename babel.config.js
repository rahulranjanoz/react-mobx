module.exports = {
    presets: [
        'next/babel',
    ],
    plugins: [
        'markdown-in-js/babel',
        'inline-react-svg',
        ['@babel/plugin-proposal-decorators', { legacy: true }],
        ['@babel/plugin-proposal-class-properties', { loose: true }],
    ],
};